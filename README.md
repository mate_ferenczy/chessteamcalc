# README #

### What is this repository for? ###

Calculate expected result between teams of chess players. Downloads teams from chess.hu.

### How do I get set up? ###

* You will need Eclipse with [e(fx)clipse](http://www.eclipse.org/efxclipse/install.html) and m2e extensions. (EGit is recommended too.)
* Java 8 is required.
* JUnit tests are available for chess classes.
* Deployment TBD