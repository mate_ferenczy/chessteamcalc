package chessHu;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import chess.ChessSeason;

@SuppressWarnings("serial")
public class ChessHuSeason extends ChessSeason {

    public ChessHuSeason(String url) {
        super(url);
    }

    @Override
    public void download(WebDriver driver) {
        //System.out.println(String.format("Downloading %s", url));
        driver.get(url);
        WebElement last = null;
        for (WebElement e : driver.findElements(By.cssSelector("a[href*='bajnoksagok.php?bajnoksag_id=']"))) {
            leagueMap.put(e.getText(), e.getAttribute("href"));
            //System.out.println("Found team: " + e.getText());
            last = e;
        }
        WebElement currentLeague = null;
        if (last == null) {
            for (WebElement e : driver.findElements(By.cssSelector("b"))) {
                if (e.getText().contains("csoport")) {
                    currentLeague = e;
                    break;
                }
            }
        } else {
            currentLeague = last.findElement(By.xpath("../b"));
        }
        leagueMap.put(currentLeague.getText(), url);
    }
}
