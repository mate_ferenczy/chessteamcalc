package chessHu;

import java.util.Date;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import chess.ChessOrg;

public class ChessHu extends ChessOrg {

    private static final long serialVersionUID = 1L;
    private transient WebDriver driver;

    public ChessHu() {
        url = "http://chess.hu/hu/bajnoksagok.php";
        uid = "chess.hu";
        name = "seasons from chess.hu";
    }

    public Service<Void> download() {
        return new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws InterruptedException {
                        updateMessage("Downloading seasons. . .");
                        driver = new HtmlUnitDriver();
                        driver.get(url);
                        seasonMap.clear();
                        WebElement last = null;
                        for (WebElement e : driver.findElements(By.cssSelector("a[href*=bajnoksag_idoszak_id]"))) {
                            ChessHuSeason season = new ChessHuSeason(e.getAttribute("href"));
                            seasonMap.put(e.getText(), season);
                            last = e;
                        }
                        ChessHuSeason season = new ChessHuSeason(url);
                        seasonMap.put(last.findElement(By.xpath("../b")).getText(), season);

                        int tc = 0;
                        updateProgress(tc++, seasonMap.size());
                        for (String s : getSeasons()) {
                            updateMessage("Downloading season " + s);
                            seasonMap.get(s).download(driver);
                            updateProgress(tc++, seasonMap.size());
                        }
                        driver.quit();
                        updateMessage("All seasons downloaded.");
                        lastDownloaded = new Date();
                        return null;
                    }
                };
            }
        };
    }
}
