package chessHu;

import java.util.Date;

import javafx.concurrent.Service;
import javafx.concurrent.Task;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import chess.ChessLeague;
import chess.ChessPlayer;
import chess.ChessTeam;

// nothing to be serialized here
@SuppressWarnings("serial")
public class ChessHuLeague extends ChessLeague {
    public ChessHuLeague(String url, String name) {
        super(url, name);
        uid = "chess.hu." + uid;
    }

    // Use the HtmlUnitDriver for performance reasons
    // We do not need JavaScript enabled for chess.hu
    private transient WebDriver driver;

    private void getLeague() {
        driver.get(url);

        // collect all Team URLs on the page
        for (WebElement TeamElement : driver.findElements(By.cssSelector("a[href*=csapat_id]"))) {
            teams.add(new ChessTeam(TeamElement.getText(), TeamElement.getAttribute("href")));
        }
    }

    private void getTeam(ChessTeam team) {
	driver.get(team.getURL());

        int numNL = 0;
        // find player names
        for (WebElement PlayerElement : driver.findElements(By.cssSelector("a[class*=NameLeft]"))) {
            // Player name is always in the first NameLeft element out of 3 elements per player
            if (numNL++ % 3 == 0) {
                team.addPlayer(new ChessPlayer(PlayerElement.getText()));
            }
        }

        int numNC = 0;
        int rating;
        // find player ratings
        for (WebElement RatingElement : driver.findElements(By.cssSelector("a[class*=NameCenter]"))) {
            // Player's Elo is always in the third NameCenter element out of 3 elements per player
            if (numNC % 3 == 2) {
                if (RatingElement.getText().isEmpty()) {
                    rating = 0;
                } else {
                    rating = Integer.parseInt(RatingElement.getText());
                }
                team.getPlayerByIndex(numNC / 3).setRating(rating);
                //System.out.println(String.format("%s : %d", Team.getPlayerByIndex(numNC / 3).getName(),
                //											Team.getPlayerByIndex(numNC / 3).getRating()));
            }
            numNC++;
        }
    }

    @Override
    public Service<Void> download() {
       return new Service<Void>() {
            @Override
            protected Task<Void> createTask() {
                return new Task<Void>() {
                    @Override
                    protected Void call() throws InterruptedException {
                        updateMessage("Downloading teams. . .");
                        driver = new HtmlUnitDriver();
                        teams.clear();
                        getLeague();
                        int tc = 0;
                        updateProgress(tc++, teams.size());
                        // open Team URLs one by one and get Players
                        for (ChessTeam Team : teams) {
                            //System.out.println(String.format("URL of team %s is: %s", Team.toString(), Team.getURL()));
                            updateMessage("Downloading " + Team.toString());
                            getTeam(Team);
                            updateProgress(tc++, teams.size());
                        }

                        //Close the browser
                        driver.quit();
                        updateMessage("All teams downloaded.");
                        lastDownloaded = new Date();
                        return null;
                    }
                };
            }
        };
    }
}
