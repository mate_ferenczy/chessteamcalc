package view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import chess.ChessPlayer;
import chess.ChessTeam;

public class TeamChosenView implements TeamViewInterface {
    private ChessTeam team;
    private ObservableMap<Integer, ChessPlayer> players;
    private PlayerCell[] cells;
    private final TeamType type;
    private final int numBoards;
    private static final Background white = new Background(new BackgroundFill(Color.web("white", 0.5), new CornerRadii(2), Insets.EMPTY));
    private static final Background black = new Background(new BackgroundFill(Color.web("lightgrey",0.5), new CornerRadii(2), Insets.EMPTY));
    
    public static
    <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
      List<T> list = new ArrayList<T>(c);
      java.util.Collections.sort(list);
      return list;
    }

    public TeamChosenView(final GridPane grid, final Insets pad, final int numBoards, 
                          final TeamType type, final ResultView result) {
        this.type = type;
        this.numBoards = numBoards;
        players = FXCollections.observableHashMap();
        cells = new PlayerCell[numBoards];

        int column = 1;
        if (type == TeamType.AWAY) {
            column = 3;
        }
        for (int i = 0; i < numBoards; i++) {
            cells[i] = new PlayerCell(this, type, getBoardColor(i));
            grid.add(cells[i].getCellGraphic(), column, i);
            cells[i].getCellGraphic().setPadding(pad);
        }

        players.addListener(new  MapChangeListener<Integer, ChessPlayer>() {
            public void onChanged(MapChangeListener.Change<? extends Integer, ? extends ChessPlayer> change) {
                onMapChange(grid, type, result, change);
            }
        });
    }
    
    private Background getBoardColor(int i) {
        if ((type == TeamType.HOME && (i % 2) == 0) ||
            (type == TeamType.AWAY && (i % 2) == 1)) {
            return white;
        } else {
            return black;
        }
    }

    private void onMapChange(final GridPane grid, final TeamType type,
            final ResultView result, final MapChangeListener.Change<? extends Integer, ? extends ChessPlayer> change) {
        Platform.runLater(new Runnable() {
            public void run() {
                int i = 0;
                int[] ratings = new int[change.getMap().size()];
                for (int key : asSortedList(change.getMap().keySet())) {
                    ChessPlayer player = change.getMap().get(key);
                    cells[i].update(player);
                    ratings[i] = player.getRating();
                    i++;
                }
                for (; i < numBoards; i++) {
                    cells[i].update(null);
                }
                result.onTeamChange(type, ratings);
            }
        });
    }

    public void setTeam(ChessTeam team) {
        if (this.team != team) {
            this.team = team;
            players.clear();
        }
    }
    
    public boolean addPlayer(ChessPlayer player) {
        if (players.size() < numBoards) {
            players.put(team.getIndex(player), player);
            return true;
        } else {
            return false;
        }
    }

    public void removePlayer(ChessPlayer player) {
        players.remove(team.getIndex(player));
    }

    public TeamViewType getViewType() {
        return TeamViewType.CHOSEN;
    }

    public boolean movePlayer(ChessPlayer player, ChessPlayer toPlayer) {
        if (!team.movePlayer(player, toPlayer)) {
            return false;
        }
        Task<Void> task = new Task<Void>() {
            protected Void call() throws Exception {
                ObservableMap<Integer, ChessPlayer> map = FXCollections.observableHashMap();;
                for (ChessPlayer p: players.values()) {
                    map.put(team.getIndex(p), p);
                }
                players.clear();
                players.putAll(map);
                return null;
            }
        };
        new Thread(task).start();
        return true;
    }
}
