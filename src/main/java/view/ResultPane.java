package view;

import org.controlsfx.control.HiddenSidesPane;

import chess.ChessTeam;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class ResultPane {
    private AnchorPane detailResult;
    HiddenSidesPane pane;
    private GridPane resultTable;
    private GridPane totalResult;
    private TeamChosenView home, away;
    private ResultView result;
    private LineChart<Number,Number> bc;
    XYChart.Series<Number,Number> resultProbs;
    private int numBoards;
    private static final Background highlight = new Background(new BackgroundFill(Color.LIGHTSKYBLUE, CornerRadii.EMPTY, Insets.EMPTY));
    private static final Insets pad = new Insets(10,10,10,0);
    private static final ResultPane INSTANCE = new ResultPane();
    
    private ResultPane() {};

    public static ResultPane getInstance() {
        return INSTANCE;
    }

    public void setNumBoards(int numBoards) {
        this.numBoards = numBoards;
    }
 
    public AnchorPane getDetailResult() {
        return detailResult;
    }
    public GridPane getResultTable() {
        return resultTable;
    }
    private void initBarChart() {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        bc = new LineChart<Number,Number>(xAxis,yAxis);
        bc.setTitle("Result Probabilities");
        bc.setBackground(highlight);
        xAxis.setLabel("Result");  
        xAxis.setTickLabelRotation(90);
        yAxis.setLabel("Probability");  
        resultProbs = new XYChart.Series<Number,Number>();
        bc.getData().add(resultProbs);
    }
    public void setDetailResult(final AnchorPane detailResult) {
        this.detailResult = detailResult;
        detailResult.setStyle("-fx-background-color: rgb(225,225,225)");
        pane = new HiddenSidesPane();
        resultTable = new GridPane();
        ColumnConstraints col0 = new ColumnConstraints(25);
        col0.setPercentWidth(5);
        ColumnConstraints col1 = new ColumnConstraints(170);
        col1.setPercentWidth(35);
        ColumnConstraints col2 = new ColumnConstraints(120);
        col2.setPercentWidth(25);
        resultTable.getColumnConstraints().addAll(col0, col1, col2, col1, col0);
        this.detailResult.getChildren().add(pane);
        pane.setContent(resultTable);
        initBarChart();
        pane.setBottom(bc);
        AnchorPane.setTopAnchor(pane, 0.0);
        AnchorPane.setBottomAnchor(pane, 0.0);
        AnchorPane.setRightAnchor(pane, 0.0);
        AnchorPane.setLeftAnchor(pane, 0.0);
        for (int i = 0; i < numBoards; i++) {
            Label l = new Label();
            l.setText(Integer.toString(i+1));
            l.setPadding(pad);
            l.setAlignment(Pos.CENTER_RIGHT);
            resultTable.add(l, 0, i);
            GridPane.setHalignment(l, HPos.RIGHT);
        }

        detailResult.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                if (isDroppable(event)) {
                    event.acceptTransferModes(TransferMode.MOVE);
                }
                event.consume();
            }
        });

        detailResult.setOnDragEntered(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                /* show to the user that it is an actual gesture target */
                if (isDroppable(event)) {
                    detailResult.setBackground(highlight);
                }
                event.consume();
            }
        });
        detailResult.setOnDragExited(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                /* show to the user that it is an actual gesture target */
                if (isDroppable(event)) {
                    detailResult.setBackground(null);
                }
                event.consume();
            }
        });
        detailResult.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (isDroppable(event)) {
                   DraggedPlayer player = (DraggedPlayer)(db.getContent(TeamMatchController.playerFormat));
                   success = addPlayer(player);
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });
    }

    private boolean isDroppable(DragEvent event) {
        if (event.getDragboard().hasContent(TeamMatchController.playerFormat)) {
            Dragboard db = event.getDragboard();
            DraggedPlayer player = (DraggedPlayer)(db.getContent(TeamMatchController.playerFormat));
            if (player.getViewType() == TeamViewType.ROSTER) {
                return true;
            }
        }
        return false;
    }
    public GridPane getTotalResult() {
        return totalResult;
    }
    public void setTotalResult(GridPane totalResult) {
        result = new ResultView(resultTable, numBoards, totalResult, resultProbs);
        home = new TeamChosenView(resultTable, pad, numBoards, TeamType.HOME, result);
        away = new TeamChosenView(resultTable, pad, numBoards, TeamType.AWAY, result);
        this.totalResult = totalResult;
    }
    public void setHomeTeam(ChessTeam team) {
        home.setTeam(team);
    }
    public void setAwayTeam(ChessTeam team) {
        away.setTeam(team);
    }

    public boolean addPlayer(DraggedPlayer player) {
        if (player.getTeamType() == TeamType.HOME) {
            return home.addPlayer(player.getChessPlayer());
        } else {
            return away.addPlayer(player.getChessPlayer());
        }
    }
}
