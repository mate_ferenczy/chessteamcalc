package view;

import javafx.scene.image.Image;

public class Language {
    private final String abbrev;
    private final Image img;

    public Image getImage() {
        return img;
    }
    public String getAbbrev() {
        return abbrev;
    }
    public Language(String abbrev, String flagFile) {
        super();
        this.abbrev = abbrev;
        img = new Image(MyController.class.getResource(flagFile).toExternalForm());
    }
}
