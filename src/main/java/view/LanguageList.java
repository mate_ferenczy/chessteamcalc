package view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class LanguageList {
    private static ObservableList<Language> list = FXCollections.observableArrayList(
            new Language("EN", "resources/us_icon.png"),
            new Language("HU", "resources/hun_icon.png"));
    
    public static ObservableList<Language> getList() {
        return list;
    }
}
