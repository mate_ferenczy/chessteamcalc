package view;

import chess.ChessPlayer;

public interface TeamViewInterface {

    void removePlayer(ChessPlayer player);
    
    TeamViewType getViewType();

    boolean movePlayer(ChessPlayer chessPlayer, ChessPlayer player);

}
