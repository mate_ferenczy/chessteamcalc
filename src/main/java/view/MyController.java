package view;

import javafx.stage.Stage;

public class MyController {
    private static final MyController INSTANCE = new MyController();
    private TeamMatchController ctrl;
    private Stage stage;

    public TeamMatchController getCtrl() {
        return ctrl;
    }

    public void setCtrl(TeamMatchController ctrl) {
        this.ctrl = ctrl;
    }

    private MyController() {};

    public static MyController getInstance() {
        return INSTANCE;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        SearchDataHandler.getInstance().setStage(stage);
        ActiveLeagueHandler.getInstance().setStage(stage);
    }
}
