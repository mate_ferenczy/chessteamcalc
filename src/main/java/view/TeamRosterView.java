package view;

import java.util.Comparator;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import chess.ChessPlayer;
import chess.ChessTeam;

public class TeamRosterView implements TeamViewInterface {

    private ComboBox<ChessTeam> teamBox;
    private ListView<ChessPlayer> playerList;
    private ObservableList<ChessPlayer> playersRest;
    private TeamType type;
    private TeamRosterView self;
    private static final Background highlight = new Background(new BackgroundFill(Color.LIGHTSKYBLUE, CornerRadii.EMPTY, Insets.EMPTY));
    private Comparator<ChessPlayer> comp = new Comparator<ChessPlayer>() {

        public int compare(ChessPlayer p1, ChessPlayer p2) {
            return teamBox.getValue().getPlayerList().indexOf(p1) -
                   teamBox.getValue().getPlayerList().indexOf(p2);
        }
    };
    
    public TeamRosterView(TeamType type) {
        this.type = type;
        self = this;
    }
    public ComboBox<ChessTeam> getTeamBox() {
        return teamBox;
    }

    public void setTeamBox(ComboBox<ChessTeam> teamBox) {
        this.teamBox = teamBox;
    }

    public void setPlayerList(final ListView<ChessPlayer> playerList) {
        this.playerList = playerList;
        playerList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE); 
        playerList.setCellFactory(playerCellFacCb);
        playerList.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                if (isDroppable(event)) {
                    event.acceptTransferModes(TransferMode.MOVE);
                }
                event.consume();
            }
        });

        playerList.setOnDragEntered(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                /* show to the user that it is an actual gesture target */
                if (isDroppable(event)) {
                    playerList.setBackground(highlight);
                }
                event.consume();
            }
        });
        playerList.setOnDragExited(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                /* show to the user that it is an actual gesture target */
                if (isDroppable(event)) {
                    playerList.setBackground(null);
                }
                event.consume();
            }
        });
        playerList.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                boolean success = false;
                if (isDroppable(event)) {
                   DraggedPlayer draggedPlayer = (DraggedPlayer)(db.getContent(TeamMatchController.playerFormat));
                   success = addPlayer(draggedPlayer.getChessPlayer());
                }
                event.setDropCompleted(success);
                event.consume();
            }
        });
        playerList.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    ChessPlayer[] players = (ChessPlayer[]) playerList.getSelectionModel().getSelectedItems().toArray(new ChessPlayer[playerList.getSelectionModel().getSelectedItems().size()]);
                    for (ChessPlayer player : players) {
                        if (ResultPane.getInstance().addPlayer(new DraggedPlayer(player, type, getViewType()))) {
                            removePlayer(player);
                        }
                    }
                    event.consume();
                } 
            }
        });
    }
    public void onAction() {
        if (teamBox.getValue() != null) {
            playersRest = teamBox.getValue().getPlayerList();
            playerList.setItems(playersRest);
        }
    }
    public void initialize() {
        teamBox.setPromptText("Choose " + type.toString() + " Team");
        teamBox.setCellFactory(new CellFactoryCallback<ChessTeam>().getCallback());
        teamBox.setDisable(true);
    }

    private boolean isDroppable(DragEvent event) {
        if (event.getDragboard().hasContent(TeamMatchController.playerFormat)) {
            Dragboard db = event.getDragboard();
            DraggedPlayer player = (DraggedPlayer)(db.getContent(TeamMatchController.playerFormat));
            if (player.getTeamType() == type &&
                player.getViewType() == TeamViewType.CHOSEN) {
                return true;
            }
        }
        return false;
    }

    private final Callback<ListView<ChessPlayer>, ListCell<ChessPlayer>> playerCellFacCb =
            new Callback<ListView<ChessPlayer>, ListCell<ChessPlayer>>() {

        public ListCell<ChessPlayer> call(ListView<ChessPlayer> param) {
            return new PlayerListCell(self, type, null);
        }
    };

    public void removePlayer(ChessPlayer player) {
        playersRest.remove(player);
    }
    public TeamViewType getViewType() {
        return TeamViewType.ROSTER;
    }

    public boolean addPlayer(ChessPlayer player) {
        playersRest.add(player);
        playersRest.sort(comp);
        return true;
    }
    public void disable() {
        teamBox.setDisable(true);
        if (playersRest != null) {
            playersRest.clear();
        }
    }
    public void enable(ObservableList<ChessTeam> teamList) {
        teamBox.setDisable(false);
        teamBox.setItems(teamList);
    }
    public boolean movePlayer(ChessPlayer player, ChessPlayer toPlayer) {
        if (!teamBox.getValue().movePlayer(player, toPlayer)) {
            return false;
        }
        playersRest.sort(comp);
        return true;
    }
}
