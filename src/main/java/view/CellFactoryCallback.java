package view;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

public class CellFactoryCallback<T> {

    public Callback<ListView<T>, ListCell<T>> getCallback() {
        return new Callback<ListView<T>, ListCell<T>>() {

            public ListCell<T> call(ListView<T> param) {
                final ListCell<T> cell = new ListCell<T>() {

                    @Override
                    protected void updateItem(T t, boolean emtpy) {
                        super.updateItem(t, emtpy);

                        if (t != null) {
                            setText(t.toString());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        };
    }
}