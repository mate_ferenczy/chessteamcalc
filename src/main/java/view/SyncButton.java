package view;

import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import org.controlsfx.glyphfont.Glyph;
import org.ocpsoft.prettytime.PrettyTime;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class SyncButton implements Observer {
    private final Button button;
    private final HBox hbox = new HBox();
    private final VBox vbox = new VBox();
    private final Glyph glyph = new Glyph("FontAwesome","REFRESH");
    private final Label icon;
    private final Label upper = new Label("Sync Search Data");
    private final Label lower = new Label();
    private static final PrettyTime pt = new PrettyTime();
    private State state = State.SEARCH;

    private enum State {
        SEARCH,
        LEAGUE
    }
    public SyncButton() {
        glyph.setFontSize(32);
        glyph.setPadding(new Insets(0,10,0,0));
        upper.setPadding(new Insets(3,0,3,0));
        lower.getStyleClass().add("sub-text");
        icon = new Label("", glyph);
        vbox.getChildren().addAll(upper, lower);
        hbox.getChildren().addAll(icon, vbox);
        button = new Button("", hbox);

        LeagueSearchBar.getInstance().addObserver(this);
        ActiveLeagueHandler.getInstance().addObserver(this);
        SearchDataHandler.getInstance().addObserver(this);

        button.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent e) {
                if (state == State.SEARCH) {
                    SearchDataHandler.getInstance().sync();
                } else {
                    ActiveLeagueHandler.getInstance().sync();
                }
            }
        });
    }

    public Button getButton() {
        return button;
    }

    public void update(Observable o, Object arg) {
        if (o instanceof LeagueSearchBar) {
            LeagueSearchBar.Event event = (LeagueSearchBar.Event) arg;
            if (event.type == LeagueSearchBar.EventType.LEAGUE_SET) {
                upper.setText("Sync " + event.leagueName);
                state = State.LEAGUE;
                return;
            } else {
                upper.setText("Sync Search Data");
                state = State.SEARCH;
            }
        } else if (o instanceof SearchDataHandler) {
            state = State.SEARCH;
        } else if (o instanceof ActiveLeagueHandler) {
            state = State.LEAGUE;
        }
        Date lastSyncDate;
        if (state == State.SEARCH) {
            lastSyncDate = SearchDataHandler.getInstance().getLastRefreshedDate();
        } else {
            lastSyncDate = ActiveLeagueHandler.getInstance().getLastRefreshedDate();
        }
        lower.setText("Last sync: " + pt.format(lastSyncDate));
    }
}