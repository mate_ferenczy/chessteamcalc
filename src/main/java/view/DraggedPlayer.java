package view;

import chess.ChessPlayer;

public class DraggedPlayer extends ChessPlayer {

    private static final long serialVersionUID = 4121407957084070770L;
    private TeamType teamType;
    private TeamViewType viewType;

    public DraggedPlayer(ChessPlayer player, TeamType teamType, TeamViewType viewType) {
        super(player);
        this.teamType = teamType;
        this.viewType = viewType;
    }

    public TeamType getTeamType() {
        return teamType;
    }
    
    public TeamViewType getViewType() {
        return viewType;
    }
    
    public ChessPlayer getChessPlayer() {
        return new ChessPlayer(getName(), getRating());
    }
}
