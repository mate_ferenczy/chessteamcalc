package view;

import java.util.Observable;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TreeItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import org.controlsfx.control.BreadCrumbBar;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.BreadCrumbBar.BreadCrumbActionEvent;
import org.controlsfx.control.PopOver.ArrowLocation;

import chess.LeagueSelector;

public class LeagueSearchBar extends Observable {
    private static LeagueSearchBar INSTANCE = null;
    private BreadCrumbBar<String> searchBar;
    private LeagueSelector selector;
    private static final TreeItem<String> root = new TreeItem<String>("Search for leagues");
    private int lastLevel = 0;
    private PopOver selectView = new PopOver();
    private ListView<TreeItem<String>> choiceList = new ListView<TreeItem<String>>();

    public enum EventType {
        LEAGUE_SET,
        LEAGUE_UNSET,
    }
    public class Event {
        public EventType type;
        public String leagueUrl;
        public String leagueName;
        public Event(EventType type, String leagueUrl, String leagueName) {
            this.type = type;
            this.leagueUrl = leagueUrl;
            this.leagueName = leagueName;
        }
    }
    public LeagueSearchBar() {
        selectView.setContentNode(choiceList);
        selectView.setAutoHide(true);
        selectView.setArrowLocation(ArrowLocation.TOP_LEFT);
        selectView.setStyle("-fx-font-size: 12pt;");
        searchBar = new BreadCrumbBar<String>(root);
        searchBar.getStyleClass().add("sub-title"); 
        searchBar.setOnCrumbAction(new EventHandler<BreadCrumbActionEvent<String>>(){
            public void handle(BreadCrumbActionEvent<String> event) {
                int level = getTreeLevel(event.getSelectedCrumb());
                if (lastLevel == 2) {
                    setChanged();
                    notifyObservers(new Event(EventType.LEAGUE_UNSET, null, null));
                }
                if (level == 0) {
                    if (root.getChildren().isEmpty()) {
                        for (String season : selector.getSeasons()) {
                            root.getChildren().add(new TreeItem<String>(season));
                        }
                    }
                    choiceList.setItems(root.getChildren());
                } else if (level == 1) {
                    selectSeason(event.getSelectedCrumb());
                }
                selectView.show(searchBar);
                lastLevel = level;
            }
        });
        choiceList.setCellFactory(new Callback<ListView<TreeItem<String>>, ListCell<TreeItem<String>>>() {
            public ListCell<TreeItem<String>> call(ListView<TreeItem<String>> param) {
                return new ListCell<TreeItem<String>>() {
                    {
                        final ListCell<TreeItem<String>> self = this;
                        setOnMouseClicked(new EventHandler<MouseEvent>() {
                            public void handle(MouseEvent mouseEvent) {
                                if (mouseEvent.getButton().equals(MouseButton.PRIMARY) && self.getText() != null && !self.getText().isEmpty()) {
                                    searchBar.setSelectedCrumb(searchBar.getSelectedCrumb().getChildren().get(self.getIndex()));
                                    if (getTreeLevel(searchBar.getSelectedCrumb()) == 1) {
                                        searchBar.fireEvent(new BreadCrumbActionEvent<String>(searchBar.getSelectedCrumb()));
                                        lastLevel = 1;
                                    } else {
                                        selectView.hide();
                                        setChanged();
                                        notifyObservers(new Event(EventType.LEAGUE_SET,
                                                selector.getLeagueUrl(searchBar.getSelectedCrumb().getParent().getValue(), self.getText()),
                                                self.getText()));
                                        lastLevel = 2;
                                    }
                                    mouseEvent.consume();
                                }
                            }
                        });
                    }
                    @Override 
                    protected void updateItem(TreeItem<String> item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText(item.getValue());
                        }
                   }
                };
            }
        });
    }

    public static LeagueSearchBar getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new LeagueSearchBar();
        }
        return INSTANCE;
    }

    private int getTreeLevel(TreeItem<String> item) {
        int level = 0;
        for (; item.getParent() != null; item = item.getParent()) {
            level++;
        }
        return level;
    }

    public void setSelector(LeagueSelector selector) {
        this.selector = selector;
    }

    private void selectSeason(TreeItem<String> treeItem) {
        if (treeItem.getChildren().isEmpty()) {
            for (String league : selector.getLeagues(treeItem.getValue())) {
                treeItem.getChildren().add(new TreeItem<String>(league));
            }
        }
        choiceList.setItems(treeItem.getChildren());
    }

    public BreadCrumbBar<String> getSearchBar() {
        return searchBar;
    }
}
