package view;

import chess.ChessPlayer;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

public class PlayerCell {

    private GridPane grid; 
    private Label text1, text2;
    private ChessPlayer player;
    private TeamViewInterface parent;
    private TeamType type;

    private static final Background highlight = new Background(new BackgroundFill(Color.LIGHTSKYBLUE, 
                                                        new CornerRadii(10), Insets.EMPTY));

    public PlayerCell(final TeamViewInterface parent, final TeamType type, final Background background) {
        this.parent = parent;
        this.type = type;
        grid = new GridPane();
        ColumnConstraints column1 = new ColumnConstraints(100,Region.USE_COMPUTED_SIZE,Double.MAX_VALUE);
        column1.setHgrow(Priority.ALWAYS);
        ColumnConstraints column2 = new ColumnConstraints(40);
        grid.getColumnConstraints().addAll(column1, column2);
        text1 = new Label();
        text1.setStyle("-fx-border-width:5; -fx-border-color:transparent; -fx-border-radius:10");
        text2 = new Label();
        grid.add(text1, 0, 0);
        grid.add(text2, 1, 0);
        grid.setBackground(background);

        grid.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent arg0) {
                grid.setBackground(highlight);
            }
        });
        grid.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent arg0) {
                grid.setBackground(background);
            }
        });
        grid.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                if (player == null) {
                    return;
                }
                Dragboard db = grid.startDragAndDrop(TransferMode.MOVE);

                ClipboardContent content = new ClipboardContent();
                content.put(TeamMatchController.playerFormat, new DraggedPlayer(player, type, getParentType()));
                db.setContent(content);

                event.consume(); 
            }
        });
        grid.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                if (event.getTransferMode() == TransferMode.MOVE) {
                    parent.removePlayer(player);
                }
            }
        });
        grid.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        movePlayer(type);
                        mouseEvent.consume();
                    }
                }
            }
        });
        grid.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                if (isDroppable(event)) {
                    event.acceptTransferModes(TransferMode.MOVE);
                    event.consume();
                }
            }
        });

        grid.setOnDragEntered(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                /* show to the user that it is an actual gesture target */
                if (isDroppable(event)) {
                    grid.setBackground(highlight);
                    event.consume();
                }
            }
        });
        grid.setOnDragExited(new EventHandler <DragEvent>() {
            public void handle(DragEvent event) {
                /* show to the user that it is an actual gesture target */
                if (isDroppable(event)) {
                    grid.setBackground(background);
                    event.consume();
                }
            }
        });
        grid.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (isDroppable(event)) {
                   DraggedPlayer draggedPlayer = (DraggedPlayer)(db.getContent(TeamMatchController.playerFormat));
                   parent.movePlayer(draggedPlayer.getChessPlayer(), player);
                   event.setDropCompleted(false);
                   event.consume();
                }
            }
        });
    }

    private boolean isDroppable(DragEvent event) {
        if (event.getDragboard().hasContent(TeamMatchController.playerFormat)) {
            Dragboard db = event.getDragboard();
            DraggedPlayer player = (DraggedPlayer)(db.getContent(TeamMatchController.playerFormat));
            if (player.getTeamType() == type &&
                player.getViewType() == getParentType()) {
                return true;
            }
        }
        return false;
    }
    
    public TeamViewType getParentType() {
        return parent.getViewType();
    }
    private void movePlayer(TeamType type) {
        if (player == null)
            return;
        if (parent.getViewType() == TeamViewType.ROSTER) {
            if (ResultPane.getInstance().addPlayer(new DraggedPlayer(player, type, getParentType()))) {
                parent.removePlayer(player);
            }
        } else {
            if (MyController.getInstance().getCtrl().getRosterView(type).addPlayer(player)) {
                parent.removePlayer(player);
            }
        }
    }
    public void update(ChessPlayer t) {
        player = t;
        if (t != null) {
            text1.setText(t.getName());
            text2.setText(Integer.toString(t.getRating()));
        } else {
            text1.setText(null);
            text2.setText(null);
        }
    }
    
    public GridPane getCellGraphic() {
        return grid;
    }
}
