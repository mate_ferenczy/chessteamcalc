package view;

import java.text.DecimalFormat;

import chess.IndividualChessGame;
import chess.TeamChessGame;
import javafx.geometry.HPos;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class ResultView {

    private int numBoards;
    private int[] homeRatings = null, awayRatings = null;
    private double[] scores;
    private Label[] labels;
    private Label homeTotal, awayTotal;
    private Series<Number, Number> resultProbs;
    private final static DecimalFormat gameFormat = new DecimalFormat("#0.00");
    private final static DecimalFormat resultFormat = new DecimalFormat("#0.0");
    
    public ResultView(GridPane resultTable, int numBoards, GridPane totalResult, Series<Number, Number> resultProbs) {
        this.numBoards = numBoards;
        this.resultProbs = resultProbs;
        labels = new Label[numBoards];
        scores = new double[numBoards];
        for (int i = 0; i < numBoards; i++) {
            labels[i] = new Label();
            resultTable.add(labels[i], 2, i);
            GridPane.setHalignment(labels[i], HPos.CENTER);
        }
        homeTotal = new Label();
        awayTotal = new Label();
        homeTotal.getStyleClass().add("title-label");
        awayTotal.getStyleClass().add("title-label");
        totalResult.add(homeTotal, 0, 0);
        totalResult.add(awayTotal, 1, 0);
        GridPane.setHalignment(homeTotal, HPos.CENTER);
        GridPane.setHalignment(awayTotal, HPos.CENTER);
    }
    
    public void onTeamChange(TeamType type, int[] ratings) {
        if (ratings.length != numBoards) {
            ratings = null;
        }
        if (type == TeamType.HOME) {
            homeRatings = ratings;
        } else {
            awayRatings = ratings;
        }
        if (homeRatings != null && awayRatings != null) {

            double totalScore = 0;
            for (int i = 0; i < numBoards; i++) {
                int white, black;
                if (i % 2 == 0) {
                    white = homeRatings[i];
                    black = awayRatings[i];
                } else {
                    black = homeRatings[i];
                    white = awayRatings[i]; 
                }
                IndividualChessGame game = new IndividualChessGame(white, black);
                if (i % 2 == 0) {
                    scores[i] = game.getExpectedScore();
                } else {
                    scores[i] = 1 - game.getExpectedScore();
                }
                labels[i].setText(gameFormat.format(scores[i]) + " - " +
                                  gameFormat.format(1-scores[i]));
                totalScore += scores[i];
            }
            homeTotal.setText(resultFormat.format(totalScore));
            awayTotal.setText(resultFormat.format(numBoards-totalScore));
            
            double[] probs = new TeamChessGame(homeRatings, awayRatings).getAllResultProbability();
            for (int i = 0; i < probs.length; i++) {
                resultProbs.getData().add(new Data<Number, Number>((double)i/2.0, probs[i]));
            }
        } else {
            for (int i = 0; i < numBoards; i++) {
                labels[i].setText(null);
            }
            homeTotal.setText(null);
            awayTotal.setText(null);
            resultProbs.getData().clear();
        }
    }

}
