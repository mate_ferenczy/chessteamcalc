package view;

import java.util.Observable;
import java.util.Observer;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.ToolBar;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import chess.ChessLeague;
import chess.ChessTeam;
import chess.ChessPlayer;

public class TeamMatchController implements Observer {

    @FXML
    private ToolBar topBar;
    @FXML
    private ComboBox<ChessTeam> homeBox, awayBox;
    @FXML
    private ListView<ChessPlayer> homeList, awayList;
    @FXML
    private AnchorPane detailResult;
    @FXML
    private GridPane totalResult;

    private final static ChessBar chessBar = new ChessBar();
    private final static TeamRosterView homeView = new TeamRosterView(TeamType.HOME);
    private final static TeamRosterView awayView = new TeamRosterView(TeamType.AWAY);
    private final static ResultPane resultPane = ResultPane.getInstance();
    public static final DataFormat playerFormat = new DataFormat("ChessPlayer");

    @FXML
    private void initialize() {
        chessBar.setTopBar(topBar);

        homeView.setTeamBox(homeBox);
        homeView.setPlayerList(homeList);
        homeView.initialize();

        awayView.setTeamBox(awayBox);
        awayView.setPlayerList(awayList);
        awayView.initialize();

        resultPane.setNumBoards(12);
        resultPane.setDetailResult(detailResult);
        resultPane.setTotalResult(totalResult);

        ActiveLeagueHandler.getInstance().addObserver(this);
        LeagueSearchBar.getInstance().addObserver(this);

        homeBox.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                resultPane.setHomeTeam(homeBox.getValue());
                homeView.onAction();
            }
        });
        awayBox.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                resultPane.setAwayTeam(awayBox.getValue());
                awayView.onAction();
            }
        });
    }

    public TeamRosterView getRosterView(TeamType type) {
        if (type == TeamType.HOME) {
            return homeView;
        } else {
            return awayView;
        }
    }
    
    public void update(Observable o, Object arg) {
        if (o instanceof LeagueSearchBar) {
            LeagueSearchBar.Event event = (LeagueSearchBar.Event) arg;
            if (event.type == LeagueSearchBar.EventType.LEAGUE_UNSET) {
                homeView.disable();
                awayView.disable();
            }
        } else if (o instanceof ActiveLeagueHandler && arg != null) {
            ChessLeague league = (ChessLeague) arg;
            homeView.enable(league.getTeamList());
            awayView.enable(league.getTeamList());
        }
    }
}
