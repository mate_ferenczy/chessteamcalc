package view;

import javafx.scene.control.ListCell;
import javafx.scene.layout.Background;
import chess.ChessPlayer;

public class PlayerListCell extends ListCell<ChessPlayer> {
    public final PlayerCell cell;
    
    public PlayerListCell(final TeamViewInterface parent, final TeamType type, Background background) {
        cell = new PlayerCell(parent, type, background);
    }

    @Override
    public void updateItem(ChessPlayer t, boolean empty) {
        super.updateItem(t, empty);

        setGraphic(null);
        if (t != null && !empty) {
            cell.update(t);
            setGraphic(cell.getCellGraphic());
        }
    }
}
