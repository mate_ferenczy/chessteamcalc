package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.util.Callback;

public class ChessBar {
    private ToolBar topBar;
    private SyncButton syncButton = new SyncButton();
    private ComboBox<Language> langBox;

    public ChessBar() {

        langBox = new ComboBox<Language>();
        langBox.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                Language activeLanguage = langBox.getValue();
                //System.out.println("New language is: " + activeLanguage.getAbbrev());
                // TODO activate new language here
            }
        });
        langBox.setValue(LanguageList.getList().get(0));
        langBox.setPrefWidth(50);
        langBox.setStyle("-fx-background-color: transparent; -fx-border-color: transparent;");
    }

    private Callback<ListView<Language>, ListCell<Language>> langCellFactory =
        new Callback<ListView<Language>, ListCell<Language>>() {
            public ListCell<Language> call(ListView<Language> p) {
                return new ListCell<Language>() {
                    private final ImageView rectangle;
                    {
                        setContentDisplay(ContentDisplay.GRAPHIC_ONLY); 
                        setBackground(topBar.getBackground());
                        rectangle = new ImageView();
                    }

                    @Override 
                    protected void updateItem(Language item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            rectangle.setImage(item.getImage());
                            setGraphic(rectangle);
                        }
                   }
              };
            }
        };
    public void setTopBar(ToolBar topBar) {
        this.topBar = topBar;

        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        spacer.setMinWidth(Region.USE_PREF_SIZE);
        spacer.setMouseTransparent(true);

        langBox.setCellFactory(langCellFactory);
        langBox.setButtonCell(langCellFactory.call(null));
        langBox.setItems(LanguageList.getList());

        topBar.getItems().addAll(LeagueSearchBar.getInstance().getSearchBar(),
                                 spacer, syncButton.getButton(), langBox);
        topBar.setPadding(new Insets(5,5,5,5));
    }
}
