package view;

import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import chess.ChessLeague;
import chessHu.ChessHuLeague;
import javafx.stage.Stage;
import javafx.util.Callback;
import main.WebObjectHandler;

public class ActiveLeagueHandler extends Observable implements Observer {
    private static final ActiveLeagueHandler INSTANCE = new ActiveLeagueHandler();
    private WebObjectHandler handler;
    private Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
        LeagueSearchBar.getInstance().addObserver(this);
    }

    public static ActiveLeagueHandler getInstance() {
        return INSTANCE;
    }

    public ActiveLeagueHandler() {}

    public Date getLastRefreshedDate() {
        return handler.getLastRefreshedDate();
    }

    public void sync() {
        handler.downloadObject(stage, new Callback<Void, Void>() {
            public Void call(Void unused) {
                setChanged();
                notifyObservers();
                return null;
            }
        });
    }

    private void setLeague(final ChessLeague league) {
        handler = new WebObjectHandler(league);
        handler.populateObject(stage, new Callback<Void, Void>() {
            public Void call(Void unused) {
                setChanged();
                notifyObservers(league);
                return null;
            }
        });
    }
    public void update(Observable o, Object arg) {
        LeagueSearchBar.Event event = (LeagueSearchBar.Event) arg;
        if (event.type == LeagueSearchBar.EventType.LEAGUE_SET) {
            setLeague(new ChessHuLeague(event.leagueUrl, event.leagueName));
        }
    }
}
