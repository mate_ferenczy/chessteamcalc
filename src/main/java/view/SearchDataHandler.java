package view;

import java.util.Date;
import java.util.Observable;

import javafx.stage.Stage;
import javafx.util.Callback;
import main.WebObjectHandler;
import chess.ChessOrg;
import chessHu.ChessHu;

public class SearchDataHandler extends Observable {

    private static final SearchDataHandler INSTANCE = new SearchDataHandler();
    private static final ChessOrg selector = new ChessHu();
    private static final WebObjectHandler handler = new WebObjectHandler(selector);
    private Stage stage;

    private SearchDataHandler() {}

    public static SearchDataHandler getInstance() {
        return INSTANCE;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
        handler.populateObject(stage, new Callback<Void, Void>() {
            public Void call(Void unused) {
                setChanged();
                notifyObservers();
                LeagueSearchBar.getInstance().setSelector(selector);
                return null;
            }
        });
    }

    public Date getLastRefreshedDate() {
        return handler.getLastRefreshedDate();
    }

    public void sync() {
        handler.downloadObject(stage, new Callback<Void, Void>() {
            public Void call(Void unused) {
                setChanged();
                notifyObservers();
                return null;
            }
        });
    }
}
