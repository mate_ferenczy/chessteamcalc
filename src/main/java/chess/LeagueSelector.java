package chess;

import javafx.collections.ObservableList;

public interface LeagueSelector {
    public ObservableList<String> getSeasons();
    public ObservableList<String> getLeagues(String season);
    public String getLeagueUrl(String season, String league);
}
