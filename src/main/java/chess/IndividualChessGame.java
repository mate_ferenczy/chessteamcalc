package chess;

public class IndividualChessGame {

    private int whiteElo, blackElo;

    private double eloFunction(double ratingDiff) {
        return (1 / (1 + Math.pow(10, ratingDiff/400)));
    }

    private static double EloAdvantage = 32.8;
    private static double EloDraw = 97.3;

    public IndividualChessGame(int whiteElo, int blackElo) {
        this.whiteElo = whiteElo;
        this.blackElo = blackElo;
    }

    public double getWhiteWinProbability() {
        return eloFunction(blackElo - whiteElo - EloAdvantage + EloDraw);
    }

    public double getDrawProbability() {
        return (1 - getWhiteWinProbability() - getBlackWinProbability());
    }

    public double getBlackWinProbability() {
        return eloFunction(whiteElo - blackElo + EloAdvantage + EloDraw);
    }

    public double getResultProbability(ChessGameResult whiteResult) {
        switch (whiteResult) {
            case WIN:
                return getWhiteWinProbability();
            case DRAW:
                return getDrawProbability();
            case LOSS:
                return getBlackWinProbability();
        }
        throw new IllegalArgumentException();
    }
    
    public double getExpectedScore() {
        return getWhiteWinProbability() + getDrawProbability() * 0.5;
    }
}
