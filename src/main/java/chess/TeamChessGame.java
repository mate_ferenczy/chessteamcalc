package chess;
import java.util.Arrays;

public class TeamChessGame {
    private int numBoards;
    private int[] homeRatings;
    private int[] awayRatings;
    private double[] resultProbabilities;
    private boolean probabilitiesCalculated;

    public TeamChessGame(int[] home, int[] away) {
        assert(home.length == away.length);
        numBoards = home.length;
        homeRatings = home;
        awayRatings = away;
        resultProbabilities = new double[numBoards*2 + 1];
        Arrays.fill(resultProbabilities, 0);
        probabilitiesCalculated = false;
    }

    public double getResultRangeProbability(int minHomeHalfPoints, int maxHomeHalfPoints) {
        if (probabilitiesCalculated == false)
            getAllResultProbability();

        double sumProbability = 0;
        for (int hhp = minHomeHalfPoints; hhp <= maxHomeHalfPoints; hhp++) {
            sumProbability += resultProbabilities[hhp];
        }
        return sumProbability;
    }

    public double[] getAllResultProbability() {
        if (probabilitiesCalculated == false) {
            ChessGameResult[] results = new ChessGameResult[numBoards];
            loopForTables(results, 0);
            probabilitiesCalculated = true;
        }
        return resultProbabilities;
    }

    private void loopForTables(ChessGameResult[] results, int tableIndex) {
        if (tableIndex == results.length) {
            calcResultElementProb(results);
        } else {
            for (ChessGameResult r : ChessGameResult.values()) {
                results[tableIndex] = r;
                loopForTables(results, tableIndex+1);
            }
        }
    }

    private void calcResultElementProb(ChessGameResult[] results) {
        double resultProbability = 1.0;
        int homeHalfPoints = 0;
        ChessGameResult whiteResult;
        int whiteRating, blackRating;

        for (int table = 0; table < results.length; table++) {
            if (table % 2 == 0) {
                whiteRating = awayRatings[table];
                blackRating = homeRatings[table];
                whiteResult = results[table].getOppositeResult();
            } else {
                whiteRating = homeRatings[table];
                blackRating = awayRatings[table];
                whiteResult = results[table];
            }
            IndividualChessGame tableGame = new IndividualChessGame(whiteRating, blackRating);
            resultProbability *= tableGame.getResultProbability(whiteResult);
            homeHalfPoints += results[table].Value();
        }
        resultProbabilities[homeHalfPoints] += resultProbability;
    }

    //TODO: the following methods should be factored out
    /*private void processSubsets(int[] set, int k) {
        int[] subset = new int[k];
        processLargerSubsets(set, subset, 0, 0);
    }

    private void processLargerSubsets(int[] set, int[] subset, int subsetSize, int nextIndex) {
        if (subsetSize == subset.length) {
            //process(subset);
        } else {
            for (int j = nextIndex; j < set.length; j++) {
                subset[subsetSize] = set[j];
                processLargerSubsets(set, subset, subsetSize + 1, j + 1);
            }
        }
    }

    // ordered sets only!
    private void subsetDiff(int[] superSet, int[] subSet, int[] resultSet) {
        assert(subSet.length + resultSet.length == superSet.length);
        int i=0, j=0, k=0;
        for ( ; i < superSet.length; i++, j++) {
            for ( ; j >= subSet.length || superSet[i] < subSet[j]; i++) {
                if (i >= superSet.length) {
                    break;
                }
                resultSet[k++] = superSet[i];
            }
        }
    }
    */
}
