package chess;
public enum ChessGameResult {
	LOSS(0), DRAW(1), WIN(2);

	private int value;

    private ChessGameResult(int value) {
            this.value = value;
    }

    public int Value() {
	return value;
    }

    public ChessGameResult getOppositeResult() {
	if (this == LOSS) {
		return WIN;
	} else if (this == WIN) {
		return LOSS;
	} else {
		return DRAW;
	}
    }
}
