package chess;

import java.io.Serializable;
import java.util.HashMap;

import org.openqa.selenium.WebDriver;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public abstract class ChessSeason implements Serializable {
    private static final long serialVersionUID = 1L;
    protected HashMap<String, String> leagueMap = new HashMap<String, String>();
    protected final String url;

    public ChessSeason(String url) {
        this.url = url;
    }

    public abstract void download(WebDriver driver);

    public String getLeagueURL(String league) {
        return leagueMap.get(league);
    }
    public ObservableList<String> getLeagues() {
        ObservableList<String> leagues = FXCollections.observableArrayList(leagueMap.keySet());
        leagues.sort(String.CASE_INSENSITIVE_ORDER);
        return leagues;
    }
}
