package chess;
import java.util.LinkedList;
import java.util.List;
import java.io.Serializable;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ChessTeam implements Serializable {

    private static final long serialVersionUID = 1L;
    private String name;
    private String url;
    private List<ChessPlayer> playerList;

    public ChessTeam(String teamName, String teamURL) {
        name = teamName;
        url = teamURL;
        playerList = new LinkedList<ChessPlayer>();
    }

    public String toString() {
        return name;
    }

    public String getURL() {
        return url;
    }

    public void addPlayer(ChessPlayer player) {
        playerList.add(player);
    }

    public ChessPlayer getPlayerByIndex(int index) {
        return playerList.get(index);
    }

    public int getIndex(ChessPlayer player) {
        return playerList.indexOf(player);
    }

    public ObservableList<ChessPlayer> getPlayerList() {
        if (playerList == null) {
            return null;
        }
        ObservableList<ChessPlayer> obsList = FXCollections.observableArrayList();
        for (ChessPlayer player : playerList) {
            obsList.add(player);
        }
        return obsList;
    }

    public boolean movePlayer(ChessPlayer player, ChessPlayer toPlayer) {
        int toIndex = getIndex(toPlayer);
        if (toIndex < 0) {
            return false;
        }
        playerList.remove(player);
        playerList.add(toIndex, player);
        return true;
    }
}
