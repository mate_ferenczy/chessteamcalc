package chess;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Date;
import java.util.HashMap;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import main.WebObject;

public abstract class ChessOrg implements WebObject, LeagueSelector {
    private static final long serialVersionUID = 1L;
    protected String url;
    protected String uid;
    protected String name;
    protected Date   lastDownloaded;
    protected HashMap<String, ChessSeason> seasonMap = new HashMap<String, ChessSeason>();

    public abstract Service<Void> download();

    public String getUid() {
        return uid;
    }
    public String getName() {
        return name;
    }
    public String getUrl() {
        return url;
    }
    public boolean downloadSuccessful() {
        return !seasonMap.isEmpty();
    }

    public Date getDownloadDate() {
        return lastDownloaded;
    }

    public void read(ObjectInputStream in) throws ClassNotFoundException,
            IOException {
        ChessOrg tmp = (ChessOrg)in.readObject();
        this.seasonMap = tmp.seasonMap;
        this.lastDownloaded = tmp.lastDownloaded;
    }

    public ObservableList<String> getSeasons() {
        ObservableList<String> seasons = FXCollections.observableArrayList(seasonMap.keySet());
        seasons.sort(String.CASE_INSENSITIVE_ORDER);
        return seasons;
    }

    public ObservableList<String> getLeagues(String season) {
        return seasonMap.get(season).getLeagues();
    }

    public String getLeagueUrl(String season, String league) {
        return seasonMap.get(season).getLeagueURL(league);
    }
}
