package chess;
import java.io.Serializable;

public class ChessPlayer implements Serializable {

    private static final long serialVersionUID = 1L;
	private String Name;
	private int Rating;

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((Name == null) ? 0 : Name.hashCode());
        result = prime * result + Rating;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChessPlayer other = (ChessPlayer) obj;
        if (Name == null) {
            if (other.Name != null)
                return false;
        } else if (!Name.equals(other.Name))
            return false;
        if (Rating != other.Rating)
            return false;
        return true;
    }
	public ChessPlayer(String playerName) {
		Name = playerName;
	}

	public ChessPlayer(String playerName, int rating) {
		Name = playerName;
		Rating = rating;
	}
	public ChessPlayer(ChessPlayer player) {
	    Name = player.getName();
	    Rating = player.getRating();
	}

	public String getName() {
		return Name;
	}
	public void setName(String playerName) {
		Name = playerName;
	}
	public int getRating() {
		return Rating;
	}
	public void setRating(int rating) {
		Rating = rating;
	}
}
