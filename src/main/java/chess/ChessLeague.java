package chess;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.WebObject;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;

public abstract class ChessLeague implements WebObject {

    private static final long serialVersionUID = 1L;
    protected final String url;
    protected String uid;
    protected final String name;
    protected Date lastDownloaded;
    protected List<ChessTeam> teams = new ArrayList<ChessTeam>();

    // website specific implementation using selenium webdriver API
    public abstract Service<Void> download();

    protected String getMatch(String text, String pattern) {
        Pattern _pattern = Pattern.compile(pattern);
        Matcher matcher = _pattern.matcher(text);
        if (matcher.find())
            return matcher.group();
        else
            return null;
    }

    // constructor gets the URL or filename for the league
    public ChessLeague(String url, String name) {
        this.url = url;
        this.name = name;
        this.uid = getMatch(url, "\\d+");
    }

    public String getUid() {
        return uid;
    }
    public String getName() {
        return "league " + name;
    }
    public String getUrl() {
        return url;
    }
    public boolean downloadSuccessful() {
        return !teams.isEmpty();
    }
    public Date getDownloadDate() {
        return lastDownloaded;
    }
    public void read(ObjectInputStream in) throws ClassNotFoundException, IOException {
        ChessLeague tmp = (ChessLeague)in.readObject();
        this.teams = tmp.teams;
        this.lastDownloaded = tmp.lastDownloaded;
    }
    public ChessTeam getTeamByName(String teamName) {
        for (ChessTeam Team : teams) {
            if (Team.toString() == teamName) {
                return Team;
            }
        }
        return null;
    }

    public ChessTeam getTeamByIndex(int index) {
        return teams.get(index);
    }

    public String getLeagueURL() {
        return url;
    }

    public ObservableList<ChessTeam> getTeamList() {
        ObservableList<ChessTeam> obsTeams = FXCollections.observableArrayList();
        for (ChessTeam team : teams) {
            obsTeams.add(team);
        }
        return obsTeams;
    }
}