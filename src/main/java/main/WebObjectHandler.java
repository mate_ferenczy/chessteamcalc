package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;

import org.controlsfx.dialog.Dialogs;

import javafx.concurrent.Service;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Callback;

@SuppressWarnings("deprecation")
public class WebObjectHandler {
    
    private final WebObject object;

    public WebObjectHandler(WebObject object) {
        this.object = object;
    }

    public void populateObject(final Stage stage, final Callback<Void, Void> callback) {
        updateObject(stage, callback, false);
    }
    
    public void downloadObject(final Stage stage, final Callback<Void, Void> callback) {
        updateObject(stage, callback, true);
    }

    private void updateObject(final Stage stage, final Callback<Void, Void> callback, boolean doDownload) {

        if (doDownload == false && new File(getFilename()).exists()) {
            try {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(getFilename()));
                object.read(in);
                in.close();
                if (callback != null) {
                    callback.call(null);
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                doDownload = true;
            }
        } else {
            doDownload = true;
        }
        if (doDownload) {
            Service<Void> service = object.download();
            Dialogs.create()
                    .owner(stage)
                    .title("Progress Dialog")
                    .masthead("Downloading " + object.getName())
                    .lightweight()
                    .showWorkerProgress(service);

            service.setOnSucceeded(new EventHandler<WorkerStateEvent>() {

                public void handle(WorkerStateEvent t) {
                    if (!object.downloadSuccessful()) {
                        Dialogs.create()
                                .owner(stage)
                                .title("Error")
                                .message("Could not get " + object.getName() + 
                                         " from " + object.getUrl());
                        return;
                    }

                    try {
                        save();
                    } catch (Exception e) {
                        Dialogs.create()
                               .owner(stage)
                               .title("Error")
                               .message("Could not save " + object.getName() +
                                        " to " + getFilename())
                               .showException(e);
                    }
                    if (callback != null) {
                        callback.call(null);
                    }
                }
            });
            service.start();
        }
    }

    public Date getLastRefreshedDate() {
        return object.getDownloadDate();
    }

    private String getDirName() {
        return System.getProperty("user.home") + File.separator + ".chessLeague";
    }
    private String getFilename() {
        return getDirName() + File.separator + object.getUid();
    }

    private void save() throws FileNotFoundException, IOException {
        File dir = new File(getDirName());
        if (!dir.exists()) {
            dir.mkdir();
        }
        ObjectOutputStream out = null;
        out = new ObjectOutputStream(new FileOutputStream(getFilename()));
        out.writeObject(object);
        out.close();
    }
}
