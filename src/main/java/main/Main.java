package main;
//import main.java.ChessHuLeague;
import java.io.IOException;

import view.MyController;
import view.TeamMatchController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/*
public class Main {
    public static void main(String[] args) {
	ChessLeague Asztalos = new ChessHuLeague("http://chess.hu/hu/bajnnoksagok.php?bajnoksag_id=121");
	Asztalos.getAllTeamLists();
    }
}
*/

public class Main extends Application {
	private VBox rootLayout;
	public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("ChessTeam Calculator");
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MyController.class.getResource("teamMatch.fxml"));
            rootLayout = (VBox) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            scene.getStylesheets().add(MyController.class.getResource("style.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();

            TeamMatchController ctrl = loader.getController();
            MyController.getInstance().setCtrl(ctrl);
            MyController.getInstance().setStage(primaryStage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}