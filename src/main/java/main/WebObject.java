package main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Date;

import javafx.concurrent.Service;

public interface WebObject extends Serializable {
    public Service<Void> download();
    public String getUid();
    public String getName();
    public String getUrl();
    public Date getDownloadDate();
    public boolean downloadSuccessful();
    public void read(ObjectInputStream in) throws ClassNotFoundException, IOException;
}
