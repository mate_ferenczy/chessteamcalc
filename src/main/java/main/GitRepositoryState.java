package main;
import java.io.IOException;
import java.util.Properties;

/**
 * Source: http://en.wikipedia.org/wiki/Singleton_pattern#Initialization_On_Demand_Holder_Idiom
 */
public class GitRepositoryState {
    private String describe;

    // Private constructor prevents instantiation from other classes
    private GitRepositoryState() {
          Properties properties = new Properties();
          try {
              properties.load(getClass().getClassLoader().getResourceAsStream("git.properties"));
              // TODO git-commit-id plugin does not generate proper describe string, hence the workaround ...
              describe = properties.get("git.commit.id.describe").toString() + "-g" +
                         properties.get("git.commit.id.abbrev").toString();
          } catch (IOException e) {
              e.printStackTrace();
              describe = "";
          }
    }

    /**
     * SingletonHolder is loaded on the first execution of Singleton.getInstance() 
     * or the first access to SingletonHolder.INSTANCE, not before.
     */
    private static class SingletonHolder {
        private static final GitRepositoryState INSTANCE = new GitRepositoryState();
    }

    public static String getDescribe() {
        return SingletonHolder.INSTANCE.describe;
    }
}
