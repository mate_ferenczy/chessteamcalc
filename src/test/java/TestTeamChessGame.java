import java.util.Arrays;

import static org.junit.Assert.*;

import org.junit.Test;

import chess.TeamChessGame;

public class TestTeamChessGame {
	
	@Test
	public void UniformEqualTeams() {
		int tables = 12;
		int[] team = new int[tables];
		Arrays.fill(team, 2000);
		TeamChessGame teamGame = new TeamChessGame(team, team);
		double[] probs = teamGame.getAllResultProbability();
		double sum = 0;
		
		for (int i = 0; i <= tables * 2; i++) {
			System.out.println(String.format("%.1f - %.1f : %f %%", i/2.0, tables-i/2.0, 100*probs[i]));
			sum += 100*probs[i];
			if (i < tables)
				assertTrue(Math.abs(probs[tables*2 - i] - probs[i]) < 1e-5);
		}
		System.out.println(String.format("TOTAL : %f %%", sum));
		assertTrue(Math.abs(100 - sum) < 1e-5);
		
		assertTrue(Math.abs(teamGame.getResultRangeProbability(0, 11) +
							teamGame.getResultRangeProbability(12, 12)/2 - 0.50) < 1e-7);
	}
}
