import java.util.Arrays;

import static org.junit.Assert.*;

import org.junit.Test;

import chess.IndividualChessGame;

public class TestIndividualChessGame {

	@Test
	public void EqualOpponents() {
		IndividualChessGame testGame = new IndividualChessGame(2000, 2033);
		
		//System.out.println(String.format("White win prob : %f", testGame.getWhiteWinProbability()));
		//System.out.println(String.format("Black win prob : %f", testGame.getBlackWinProbability()));
		assertTrue(testGame.getWhiteWinProbability() - testGame.getBlackWinProbability() <  0.001);
		assertTrue(testGame.getWhiteWinProbability() - testGame.getBlackWinProbability() > -0.001);
		assertTrue(testGame.getWhiteWinProbability() + testGame.getBlackWinProbability() + testGame.getDrawProbability() > 0.9999);
	}
	
	@Test
	public void WhiteStrongerBy200() {
		IndividualChessGame testGame = new IndividualChessGame(2200, 2033);
		//System.out.println(String.format("White win prob : %f", testGame.getWhiteWinProbability()));
		//System.out.println(String.format("Draw prob : %f", testGame.getDrawProbability()));
		assertTrue(testGame.getWhiteWinProbability() + 0.5*testGame.getDrawProbability() > 0.74);
		assertTrue(testGame.getWhiteWinProbability() + 0.5*testGame.getDrawProbability() < 0.76);
		assertTrue(testGame.getWhiteWinProbability() + testGame.getBlackWinProbability() + testGame.getDrawProbability() > 0.9999);
	}		
	
	@Test
	public void WhiteMuchStronger() {
		IndividualChessGame testGame = new IndividualChessGame(2600, 200);
		//System.out.println(String.format("White win prob : %f", testGame.getWhiteWinProbability()));
		//System.out.println(String.format("Draw prob : %f", testGame.getDrawProbability()));
		assertTrue(testGame.getWhiteWinProbability() + 0.5*testGame.getDrawProbability() > 0.95);
		assertTrue(testGame.getWhiteWinProbability() + testGame.getBlackWinProbability() + testGame.getDrawProbability() > 0.9999);
	}
	
	@Test
	public void AbsoluteRatingIndependence() {
		IndividualChessGame testGame1 = new IndividualChessGame(2200, 2280);
		IndividualChessGame testGame2 = new IndividualChessGame(1800, 1880);
		
		assertTrue(testGame1.getWhiteWinProbability() == testGame2.getWhiteWinProbability());
		assertTrue(testGame1.getDrawProbability() == testGame2.getDrawProbability());
	}
	
	//TODO: the following methods should be factored out
	private void processSubsets(int[] set, int k) {
	    int[] subset = new int[k];
	    processLargerSubsets(set, subset, 0, 0);
	}

	private void processLargerSubsets(int[] set, int[] subset, int subsetSize, int nextIndex) {
	    if (subsetSize == subset.length) {
	        process(subset);
	    } else {
	        for (int j = nextIndex; j < set.length; j++) {
	            subset[subsetSize] = set[j];
	            processLargerSubsets(set, subset, subsetSize + 1, j + 1);
	        }
	    }
	}
	
	static void process(int[] subset) {
	    //System.out.println(Arrays.toString(subset));
	}
	
	@Test
	public void subsets() {
		int[] set = {1,2,3,4,5};
		processSubsets(set, 3);
	}
	
	private void subsetDiff(int[] minuendSet, int[] subtrahendSet, int[] resultSet) {
		assert(subtrahendSet.length + resultSet.length == minuendSet.length);
		int i=0, j=0, k=0;
		for ( ; i < minuendSet.length; i++, j++) {
			for ( ; j >= subtrahendSet.length || minuendSet[i] < subtrahendSet[j]; i++) {
				if (i >= minuendSet.length) {
					break;
				}
				resultSet[k++] = minuendSet[i];
			}
		}
	}
	
	@Test
	public void diffSubsets() {
		int[] set1 = {1,2,3,4,5};
		int[] set2 = {1,4};
		int[] set3 = new int[set1.length-set2.length];
		subsetDiff(set1, set2, set3);
		System.out.println(Arrays.toString(set3));
	}
	
}
